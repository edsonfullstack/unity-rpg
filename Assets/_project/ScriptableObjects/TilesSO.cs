using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DesignPatterns.ScriptableObjects;
using Extensions;
using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Tile", menuName = "ScriptableObjects/Collections/Tile")]
public class TilesSO : ScriptableObject
{
    [ShowInInspector]
    public Dictionary<string,int> stats;
    [ValueDropdown("@Enums.GetLayers()")]
    public int layer=0;
    [SerializeField,ShowInInspector]
    Sprite[] sprite;
    [SerializeField]
    public SpriteRenderer renderer;
    public Sprite GetRandomSprite => sprite[0.Random(0,sprite.Length-1)];
    public Sprite GetRandomWeightSprite => sprite[0.RandomWeight(0,sprite.Length-1)];
    
}
