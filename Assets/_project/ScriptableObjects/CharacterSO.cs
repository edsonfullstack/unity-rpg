using System;
using System.Collections.Generic;
using DesignPatterns.ScriptableObjects;
using Sirenix.OdinInspector;
using Unity.Plastic.Newtonsoft.Json;
using UnityEngine;

namespace _project.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Character", menuName = "ScriptableObjects/Collections/Character")]
    public class CharacterSO : DropSO
    {
        [TabGroup("Stats")]
        [ShowInInspector, JsonProperty]
        private Dictionary<string,int> stats;
        [ValueDropdown("@Enums.GetArray(typeof(StageTypes))"),ShowInInspector, JsonProperty] 
        private string type;
        [SerializeField, JsonProperty]
        private int move=1;
        [SerializeField, JsonProperty]
        private int speed=1;
        [SerializeField, JsonProperty]
        private int attack=1;
        [SerializeField, JsonProperty]
        private int defense=1;
        [SerializeField, JsonProperty]
        private int evasion=1;
        [SerializeField, JsonProperty]
        private int retaliation=1;
        [SerializeField, JsonProperty]
        private string description;
    }
}
