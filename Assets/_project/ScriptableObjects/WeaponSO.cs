using Extensions;
using DesignPatterns.ScriptableObjects;
using Sirenix.OdinInspector;
using Unity.Plastic.Newtonsoft.Json;
using UnityEngine;

namespace _project.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Collections/Weapon")]
    public class WeaponSO : DropSO
    {
        [HorizontalGroup("View", LabelWidth = 40), PreviewField(30), JsonIgnore, ShowInInspector]
        public Sprite sprite;

        [HorizontalGroup("View", LabelWidth = 40), PreviewField(30), JsonIgnore, ShowInInspector]
        public GameObject prefab;

        [SerializeField, JsonProperty] 
        private short damageMin = 0;
        [SerializeField, JsonProperty] 
        private short damageMax = 0;
        [SerializeField, JsonProperty]
        private short damageAdd = 0;
        [SerializeField, JsonProperty] 
        private short rangeMin = 0;
        [SerializeField, JsonProperty] 
        private short rangeMax = 0;
        [SerializeField, JsonProperty] 
        private short durability = 0;

        public int GetDamage()
        {
            return 0.Random(damageMin, damageMax) + damageAdd;
        }

    }
}