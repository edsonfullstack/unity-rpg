﻿using Extensions;
using Sirenix.OdinInspector;
using UnityEngine;

namespace _project.ScriptableObjects
{
    [CreateAssetMenu(fileName = "MathManager", menuName = "Systems/Math")]
    public class MathSO : ScriptableObject
    {
        public string expression ="{0}+{1}+{2}" ;
        public int level = 1;
        public int value = 0;
        [Button("expresion")]
         public void Expresion()
        {
            value = (int) expression.FromValues(level,1.0f,-100);
        } 
        [Button("RandNum")]
        public void Rad2()
        {
            Debug.Log(0.Random(7.0f,8.0f));
        }
    }
}