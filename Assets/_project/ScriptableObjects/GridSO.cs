﻿using Extensions;
using DesignPatterns.ScriptableObjects;
using _project.Scripts;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Transform))]
[CreateAssetMenu(fileName = "GridManager", menuName = "ScriptableObjects/Manager/Grid")]
public class GridSO : SingletonSO<GridSO>
{
    [ShowInInspector,SerializeField]
    public Transform basePosition;

    private GameObject go, obs;

    [SerializeField]
    public Transform cam;
    [SerializeField]
    public TilesSO[] tiles;
    [SerializeField]
    private TilesSO obstacles;
    [SerializeField,Tooltip("Y")]
    private int height;
    [SerializeField,Tooltip("X")]
    private int width;
    
    [Button("Delete")]
    void Delete()
    {
        for (int i = basePosition.transform.childCount - 1; i >= 0; i--)
            DestroyImmediate(basePosition.transform.GetChild(i).gameObject
                ,true);
    }

    [Button("Adjust CAM")]
    void Adjust()
    {
        cam.transform.position = new Vector3((float)width/2-0.5f,(float)height/2-0.5f,-10);
    }
    [Button("Generate")]
    void Generate()
    {
        var start = "--Manager--/--GridManager--";
        var maincam = "--MainCamera--";
        var gridLabel= "--Grid--";
        var obstacleLabel = "--Obstacles--";
        go = new GameObject(gridLabel);
        obs = new GameObject(obstacleLabel);
        basePosition = GameObject.Find(start).transform ? 
            GameObject.Find(start).transform : default;
        cam = GameObject.Find(maincam).transform ? 
                    GameObject.Find(maincam).transform : default;
        Adjust();
        Delete();
        go.transform.parent = basePosition.transform;
        obs.transform.parent = basePosition.transform;
        for (var y = 0; y < height; y++)
        {
            for (var x = 0; x < width; x++)
            {
                var tile = tiles[0.RandomWeight(0, tiles.Length-1)];
                var spawTile= Instantiate(tile.renderer, 
                    new Vector3(x,y),
                    Quaternion.identity,
                    go.transform);
                spawTile.name = $"{x}-{y}";
                spawTile.sprite = tile?.GetRandomSprite;
                spawTile.GetComponent<TileBehaviour>().tile= tile;
                spawTile.gameObject.layer = tile.layer;
                //var isOffset = (x % 2 == 0 && y % 2 != 0) || (x % 2 != 0 && y % 2 == 0);
            }
        }
    }

    [Button("Random Tree")]
    void RandomTree()
    {
        if (obs == null)
            obs= GameObject.Find("--Obstacles--");
        var tile = tiles[0.RandomWeight(0, tiles.Length-1)];
        var pos = new Vector3Int(0.Random(0, width - 1), 0.Random(0, height - 1), -1);
        var spawTile = Instantiate(tile.renderer,
         new Vector3(pos.x, pos.y, pos.z),
               Quaternion.identity,
               obs.transform);
        var random = obstacles.GetRandomWeightSprite;
        spawTile.sprite = random;
        spawTile.name = $"{pos.x}-{pos.y}.{random.name}";
        spawTile.gameObject.layer = obstacles.layer;
    }
}
