﻿namespace _project.Scripts.Interfaces
{
    public interface IHooverEvent
    {
        void OnMouseEnter();
        void OnMouseExit();
    }
}