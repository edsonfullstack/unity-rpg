﻿namespace _project.Scripts.Interfaces
{
    public interface ISelectEvent
    {
        void OnSelectEnter();
        void OnSelectExit();
    }
}