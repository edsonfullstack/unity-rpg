﻿using System;
using static Logger;
using _project.Scripts.Interfaces;
using UnityEngine;

namespace _project.Scripts
{
    public class TileBehaviour : MonoBehaviour, IHooverEvent
    {
        [SerializeField] private float hooverScale;
        [SerializeField] public TilesSO tile;
        

        public void OnMouseEnter()
        {
            Log($"You get a {gameObject.name}");
            transform.localScale += Vector3.one * hooverScale;
            transform.GetComponent<SpriteRenderer>().color=Color.cyan;
        }

        public void OnMouseExit()
        {
            transform.localScale -= Vector3.one * hooverScale;
            transform.GetComponent<SpriteRenderer>().color=Color.white;
        }

        public bool isClear() => null == Physics2D.OverlapCircle(transform.position, 0.2f);

    }
}