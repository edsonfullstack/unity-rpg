﻿using _project.Scripts.Interfaces;
using UnityEngine;

namespace _project.Scripts
{
    public class CharacterBehaviour : MonoBehaviour, IHooverEvent
    {
        [SerializeField] private float hooverScale;

        public void OnMouseEnter()
        {
            Debug.Log("MOUSE ENTER");
            transform.localScale += Vector3.one * hooverScale;
        }

        public void OnMouseExit()
        {
            transform.localScale -= Vector3.one * hooverScale;
        }
    }
}