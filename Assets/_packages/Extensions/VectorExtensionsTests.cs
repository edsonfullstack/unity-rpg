﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Extensions
{
    public class VectorExtensionsTests
    {
        [Test]
        public void Vector2toVector3_0(){
            Assert.AreEqual(new Vector2(0,0).ToXY(),new Vector3(0,0,0));
            Assert.AreEqual(new Vector2(0,0).ToXZ(),new Vector3(0,0,0));
        }
        [Test]
        public void Vector2toVector3_1(){
            Assert.AreEqual(new Vector2(1,1).ToXY(),new Vector3(1,1,0));
            Assert.AreEqual(new Vector2(1,1).ToXZ(),new Vector3(1,0,1));
        }
        [Test]
        public void Vector3toVector2_0(){
            Assert.AreEqual(new Vector3(0,0,0).ToXY(),new Vector2(0,0));
            Assert.AreEqual(new Vector3(0,0,0).ToXZ(),new Vector2(0,0));
        }
        [Test]
        public void Vector3toVector2_1(){
            Assert.AreEqual(new Vector3(0,1,0).ToXY(),new Vector2(0,1));
            Assert.AreEqual(new Vector3(0,1,0).ToXZ(),new Vector2(0,0));
        }
        private static IEnumerable<TestCaseData> Vector2Points()
        {
            yield return new TestCaseData(new Vector2(0,0),new Vector2(0,0),0f).SetName("0,0");
            yield return new TestCaseData(new Vector2(0,0),new Vector2(1,1),1.414214f).SetName("0,1");
            yield return new TestCaseData(new Vector2(0,0),new Vector2(10,10),14.142136f).SetName("0,10");
        }
        private static IEnumerable<TestCaseData> Vector3Points()
        {
            yield return new TestCaseData(new Vector3(0,0,0),new Vector3(0,0,0),0f).SetName("0,0");
            yield return new TestCaseData(new Vector3(0,0,0),new Vector3(1,1,1),1.732051f).SetName("0,1");
            yield return new TestCaseData(new Vector3(0,0,0),new Vector3(10,10,10),17.320508f).SetName("0,10");
        }
        [Test]
        [TestCaseSource(nameof(Vector2Points))]
        public void Vector2Distance(Vector2 start,Vector2 end,float distance){
            Assert.That(distance,Is.EqualTo(start.Distance(end)).Within(0.0001f));
        }
        [Test]
        [TestCaseSource(nameof(Vector3Points))]
        public void Vector3Distance(Vector3 start,Vector3 end,float distance){
            Assert.That(distance,Is.EqualTo(start.Distance(end)).Within(0.0001f));
        }
    }
}