﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;



namespace Extensions
{
    public static class Extensions
    {
        
        #region Enum
        public static Array ToArray(this Type e)
        {
            if (!e.IsEnum)
                throw new ArgumentException("Must be an enumerated type");
            return Enums.GetArray(e);
        }
        #endregion
        public static T Clone<T>(this T original)
        {
            System.Reflection.MethodInfo inst = original.GetType()
                .GetMethod("MemberwiseClone",
                System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
      
            return (T)inst?.Invoke(original, null);
        }
        /// <summary>
        /// This object need be serializable
        /// </summary>
        public static T CloneDeep<T>(this T original) where T :new()
        {
            if(!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable");
            }
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, original);
                ms.Position = 0;
                return (T) formatter.Deserialize(ms);
            }

        }

    }
}