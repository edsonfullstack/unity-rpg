using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Extensions;
using NUnit.Framework;
using UnityEditor;

namespace Extensions
{
    public class MathExtensionsTests
    {
        [Test]
        [TestCase(0f,0)]
        [TestCase(0.25f,25)]
        [TestCase(1,100)]
         public void ExtensionsTestsExpressionPecentual(float input, IComparable output)
        {
            Assert.AreEqual(output,100f.Percent(input));
        }
         [Test]
         [TestCase(0,0)]
         [TestCase(1,1)]
         [TestCase(25,25)]
         public void ExtensionsTestsExpressionPecentualInt(int input, IComparable output)
         {
             Assert.AreEqual(output,100.Percent(input));
         }

        [Test]
        [TestCase("{0}+{1}+{2}", new float[] {1.1f, 1.1f, 1.1f}, 3.3f,TestName="SUM 3 Floats")]
        [TestCase("{0}*{1}", new float[] {-1.1f, -1.1f}, 1.21f,TestName="Multiply Negatives")]
        [TestCase("({0}^{1})*{2}", new float[] {2.1f, 2.1f, 2.1f}, 9.9742403f,TestName="Power then Multiply")]
        public void ExtensionsTestsExpressionMustAddFloat(string expression, float[] values, float result)
        {
            var response = expression.FromValues(values);
            Assert.AreEqual(result, response);
        }

        [Test]
        [TestCase("{0}+{1}+{2}", new int[] {1, 1, 2}, 4)]
        public void ExtensionsTestsExpressionMustAddInt(string expression, int[] values, int result)
        {
            var response = expression.FromValues(values);
            Assert.AreEqual(result, response);
        }

        private static object[] CaseSource =
        {
            new object[]
                {"{força}+{dex}+{intel}", new Dictionary<string, int>() {{"força", 1}, {"dex", 1}, {"intel", 2}}, 4},
            new object[]
                {"{forca}-{dex}-{intel}", new Dictionary<string, int>() {{"forca", 1}, {"dex", 1}, {"intel", 0}}, 0},
            new object[]
                {"{forca}*{dex}+{intel}", new Dictionary<string, int>() {{"forca", 1}, {"dex", 0}, {"intel", 0}}, 0},
            new object[] {"{forca}", new Dictionary<string, int>() {{"forca", 7}, {"dex", 0}, {"intel", 0}}, 7}
        };

        [Test]
        [TestCaseSource(nameof(CaseSource))]
        public void ExtensionsTestsExpressionMustAddIntNamedValues(string expression, Dictionary<string, int> values,
            int result)
        {
            var response = expression.FromValues(values);
            Assert.AreEqual(result, response);
        }

        private static IEnumerable<TestCaseData> CaseSourceError()
        {
            yield return new TestCaseData("{forca}+{globo}", new Dictionary<string, int>() {{"forca", 7}, {"dex", 0}, {"intel", 0}}, 0
            ).SetName("Expression Attr Does Not Exist");
        }
        [Test]
        [TestCaseSource(nameof(CaseSourceError))]
        public void ExtensionsTestsExpressionMustAddIntNamedValuesError(string expression,Dictionary<string,int> values, int result)
        {
            Assert.Throws<ArgumentException>(() => expression.FromValues(values));//0 representa erro logo falho por algum index
        }
        

        [Test]
        [TestCase("{0}+{1}+{2}", new double[] {1.1d, 1.2, 2.3}, 4.6)]
        public void ExtensionsTestsExpressionMustAddDouble(string expression, double[] values, double result)
        {
            var response = expression.FromValues(values);
            Assert.AreEqual(result, response);
        }

        [Test]
        [TestCase(6, -2)]
        [TestCase(8, -1)]
        [TestCase(9, -1)]
        [TestCase(10, 0)]
        [TestCase(11, 0)]
        [TestCase(12, 1)]
        [TestCase(13, 1)]
        [TestCase(14, 2)]
        public void ExtensionsTestsExpressionDnDMode(int value, int response)
        {
            var result = value.DnDMode();
            Assert.AreEqual(result, response);
        }

        [Test]
        [TestCase(1, 10)]
        [TestCase(1.0f, 10.0f)]
        public void ExtensionsTestsRandomInt<T>(T min, T max)
        {
            var result = 0.Random(min, max);
            Assert.NotZero(result);
        }
        [Test]
        [TestCase(1, 10)]
        [TestCase(1.0f, 10.0f)]
        public void ExtensionsTestsRandomIntWeight<T>(T min, T max)
        {
            var result = 0.RandomWeight(min, max);
            Assert.NotZero(result);
        }

        [Test]
        [TestCase(1, 10)]
        [TestCase(1.0f, 10.0f)]
        public void ExtensionsTestsRandomFloat<T>(T min, T max)
        {
            var result = 0.0f.Random(min, max);
            Assert.NotZero(result);
        }

        [Test]
        public void ExtensionsTestsRandomMustBeTen()
        {
            var result = 10.Random(0, 0);
            Assert.AreEqual(result, 10);
        }

    }
}