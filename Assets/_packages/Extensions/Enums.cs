using System;
using System.Collections;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;

namespace Extensions
{
    public static class Enums
    {
        public static ValueDropdownList<int> GetLayers()
        {
            var list = new ValueDropdownList<int>();
            for (int i = 0; i < 32; i++)
            {
                if(LayerMask.LayerToName(i) != "")list.Add(LayerMask.LayerToName(i),i);
            }
            return list;
        }
        public static ValueDropdownList<int> GetList(Type type)
        {
            var list = new ValueDropdownList<int>();
            foreach (var value in type.GetEnumValues())
            {
                list.Add(value.ToString(),(int)value);
            }

            return list;
        }
        public static string[] GetArray(Type type)
        {
            return Enum.GetValues(type)
                .Cast<Enum>()
                .Select(item => item.ToString())
                .ToArray();
        }
        public static string Validate<T>(string value)
        {
            if (GetArray(typeof(T)).Contains(value))
                return value;
            throw new ArgumentException($"Enum {value} not present in {typeof(T)}");
        }
    }
}