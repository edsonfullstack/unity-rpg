﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

namespace Extensions
{
    public static class MathExtensions
    {

        #region Math
        public static IComparable Percent<T>(this T value,IComparable percentual) where T : IComparable<T>
        {
            IComparable result = default;
            if (percentual is float || percentual is double)
                result = value * (dynamic)percentual;
            if (percentual is int)
                result = value * ((dynamic)percentual/100.0f);
            return result;
        }
        public static int Random<T>(this int add,T min, T max)
        {
            return UnityEngine.Random.Range(Int32.Parse(min.ToString()), Int32.Parse(max.ToString())+1)+add;
        }
        public static int RandomWeight<T>(this int add,T min, T max)
        {
            var result = 0;
            for (int i = Int32.Parse(min.ToString()); i < Int32.Parse(max.ToString())+1 ; i++)
            { 
                var random = UnityEngine.Random.Range(0, 100);
                if ( random < 50)
                {
                    result = i;
                    break;
                }
            }

            return result;
        }
        
        public static float Random<T>(this float add,T min, T max)
        {

            return UnityEngine.Random.Range(float.Parse(min.ToString()), float.Parse(max.ToString()))+add;
        }
        public static int DnDMode(this int value,int div=2,int skip=5){
            return (value / div) - skip;
        }

        /// <summary>
        /// return 0 if its a failed expression
        /// </summary>
        public static T FromValues<T>(this string expression,params T[] values)
        {
            T result;
            ExpressionEvaluator.Evaluate(string.Format(expression,
                values.Cast<T>().Select(x => x.ToString()).ToArray()), out result);
            return result;
        }
        public static T FromValues<T>(this string expression,Dictionary<string,T> values)
        {
            T result;
            ExpressionEvaluator.Evaluate(values.ReplaceKeyInString(expression), out result);
            return result;
        }
        public static string ReplaceKeyInString<T>(this Dictionary<string, T> dictionary, string inputString)
        {

            var regex = new Regex("{(.*?)}");
            var matches = regex.Matches(inputString);
            foreach (Match match in matches)
            {
                var valueWithoutBrackets = match.Groups[1].Value;
                var valueWithBrackets = match.Value;

                if(dictionary.ContainsKey(valueWithoutBrackets))
                    inputString = inputString.Replace(valueWithBrackets, dictionary[valueWithoutBrackets].ToString());
                else
                    throw new ArgumentException($"Index value {valueWithoutBrackets} not found");
            }

            return inputString;
        }
        #endregion
    }
}