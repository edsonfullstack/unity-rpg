using System;
using NUnit.Framework;
using Extensions;

namespace Extensions
{
    public class EnumsTests
    {
        private enum StageTypes
        {
            Develop,
            Test,
            Production
        }

        [Test]
        [TestCase(typeof(StageTypes), new string[] {"Develop", "Test", "Production"})]
        public void EnumConversion(Type name, string[] response)
        {
            var result = Enums.GetArray(name);
            Assert.AreEqual(result, response);
        }

        [Test]
        [TestCase("Develop", "Develop")]
        public void EnumValidade(string input, string response)
        {
            var result = Enums.Validate<StageTypes>(input);
            Assert.AreEqual(result, response);
        }

        [Test]
        [TestCase("Dev")]
        public void EnumValidadeFail(string input)
        {
            Assert.Throws<ArgumentException>(() => Enums.Validate<StageTypes>(input));
        }

        [Test]
        public void EnumFail()
        {
            Assert.Throws<ArgumentException>(() => typeof(Enums).ToArray());
        }

        [Test]
        public void EnumSuccess()
        {
            Assert.DoesNotThrow(() => typeof(StageTypes).ToArray());
        }
        [Test]
        public void EnumSuccessList()
        {
            Assert.DoesNotThrow(() => Enums.GetList(typeof(StageTypes)));
        }
        [Test]
        public void EnumLayers()
        {
            Assert.DoesNotThrow(() => Enums.GetLayers());
        }
        
    }
}