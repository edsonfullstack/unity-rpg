﻿using UnityEngine;

namespace Extensions
{
    public static class VectorExtensions
    {

        #region Vector
        public static Vector2 ToXY(this Vector3 input) => new Vector2(input.x, input.y);
        public static Vector2 ToXZ(this Vector3 input) => new Vector2(input.x, input.z);
        public static Vector3 ToXY(this Vector2 input) => new Vector3(input.x, input.y,0);
        public static Vector3 ToXZ(this Vector2 input) => new Vector3(input.x,0, input.y);
        public static float Distance(this Vector2 input,Vector2 target) => (input-target).magnitude;
        public static float Distance(this Vector3 input,Vector3 target) => (input-target).magnitude;
        #endregion
    }
}