﻿using System;
using Extensions;
using NUnit.Framework;
using Object = System.Object;

namespace Extensions
{
    public class ExtensionsTests
    {
        [Serializable]
        private class Test
        {
            public int str;
        }

        private class TestExeption
        {
            public int str;
        }

        [Test]
        public void ExtensionsTestsClone()
        {
            var result = new Test();
            var result2 = result.Clone();
            result.str = 11;
            Assert.AreNotEqual(result, result2);
            Assert.AreNotSame(result, result2);
        }

        [Test]
        public void ExtensionsTestsCloneDeep()
        {
            var result = new Test();
            var result2 = result.CloneDeep();
            result.str = 11;
            Assert.AreNotEqual(result, result2);
            Assert.AreNotSame(result, result2);
        }

        [Test]
        public void ExtensionsTestsCloneDeepExeption()
        {
            var result = new TestExeption();
            Assert.Throws<ArgumentException>(() => result.CloneDeep());
        }
    }
}