﻿using DesignPatterns;
using DesignPatterns.ScriptableObjects;
using NUnit.Framework;
using UnityEngine;

namespace DesignPatterns
{
    public class BuilderTests
    {
        private interface ITest
        {
            void Log()
            {
                Debug.Log("Test");
            }
        }
        private class Test : Builder<Test>,ITest
        {
            public int str;
            public string dex;
        }

        [Test]
        public void BuilderInterface()
        {
            var builder = new Test().New()
                .Set(nameof(Test.str), 1)
                .Set(nameof(Test.dex), "2")
                .Build();
            ((ITest) builder).Log();
            Assert.AreEqual(typeof(Test), builder.GetType());
            Assert.AreEqual(1, builder.str);
            Assert.AreEqual("2", builder.dex);
            Assert.NotNull(builder.DebugMethods());
        }
        
    }
}