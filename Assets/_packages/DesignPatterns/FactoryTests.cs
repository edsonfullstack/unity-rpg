﻿using System;
using System.Collections.Generic;
using DesignPatterns;
using Extensions;
using NUnit.Framework;
using UnityEngine;

namespace DesignPatterns
{
    public class FactoryTests
    {
        private interface ITest
        {
            int value { get; }
        }

        private class Test : ITest
        {
            public int value { get; }
            public Test() {}
            public Test(int i)
            {
                value = i;
            }
        }

        private class Test2 : Test
        {
        }

        [Test]
        public void FactoryConstructorMustReturnTypeByName()
        {
            var instance = new Factory<ITest>().GetInstance("Test");
            Assert.AreEqual(typeof(Test), instance.GetType());
        }

        [Test]
        public void FactoryInstanceMustHaveAllInterfaceReferences()
        {
            var factory = new Factory<ITest>();
            Assert.AreEqual(factory.instances.Count, 2);
        }

        [Test]
        public void FactoryBuildByDict()
        {
            var factory = new Factory<ITest>(new Dictionary<string, ITest>()
            {
                {"Test13", new Test()},
                {"Test23", new Test2()}
            });
            Assert.AreEqual(factory.instances.Count, 2);
            Assert.Throws<ArgumentException>(() => factory.GetInstance("Test"));
        }
        [Test]
        public void FactoryBuildByDictAndConstructor()
        {
            var factory = new Factory<ITest>(new Dictionary<string, ITest>()
            {
                {"Test1", new Test(1)},
                {"Test2", new Test(2)}
            });
            Assert.AreEqual(factory.instances.Count, 2);
            var i1 = factory.GetInstance("Test1");
            var i2 = factory.GetInstance("Test2");
            Assert.AreEqual(i1.value, 1);
            Assert.AreEqual(i2.value, 2);
            Assert.AreNotSame(i1, i2);

        }

    }
}