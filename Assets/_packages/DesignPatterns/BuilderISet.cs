﻿using System;
using UnityEngine;

namespace DesignPatterns
{
    public interface BuilderISet<T>
    {
        public abstract BuilderISet<T> New();
        public BuilderISet<T> Set(string name, object value)
        {
            var info = typeof(T).GetProperty(name);
            info?.SetValue(this, Convert.ChangeType(value,value.GetType()));
            if (info == null)
            {
                var info2 = typeof(T).GetField(name);
                info2?.SetValue(this, Convert.ChangeType(value,value.GetType()));
            }
            return this;
        }
        public T Build()
        {
            return (T)this;
        }
    }
}