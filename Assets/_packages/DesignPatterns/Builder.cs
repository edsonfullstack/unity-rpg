﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Reflection;
using System.Text;
using DesignPatterns.ScriptableObjects;
using UnityEngine;

namespace DesignPatterns
{
    public abstract class Builder<T> : DynamicObject,BuilderISet<T>
    {
        public BuilderISet<T> New()
        {
            return this;
        }
        
        public string DebugMethods()
        {
            var result = new StringBuilder();
            var info = typeof(T).GetMembers();
            foreach (var x in info)
            {
                if (x.MemberType == MemberTypes.Field || x.MemberType == MemberTypes.Property ) 
                    result.Append(x?.Name + " " + x?.MemberType);
            }

            return result.ToString();
        }
        
    }
}