﻿using System;
using System.Collections.Generic;
using System.Linq;
using Extensions;

namespace DesignPatterns
{
    public class Factory<T> where T : class
    {
        
        public readonly Dictionary<string, T> instances = new();
        public Factory()
        {
            foreach (var type in typeof(T).Assembly.GetTypes())
            {
                if (typeof(T).IsAssignableFrom(type) && !type.IsInterface)
                {
                    var name = type.ToString().Split(new Char[] {'.', '+'}).Last();
                    instances.Add(name ,
                        Activator.CreateInstance(type) as T);
                }
            }
        }

        public Factory(Dictionary<string,T> dict)
        {
            instances = dict;
        }
        public T GetInstance(string key)
        {
            if (instances.ContainsKey(key))
                return instances[key].Clone();
            
            throw new ArgumentException("Key does not exist!");
        }
    }

}