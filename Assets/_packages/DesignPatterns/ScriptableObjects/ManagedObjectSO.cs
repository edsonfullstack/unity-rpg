using Sirenix.OdinInspector;
using UnityEditor;

namespace DesignPatterns.ScriptableObjects
{
#if UNITY_EDITOR
#endif
 
    [InitializeOnLoad,ShowOdinSerializedPropertiesInInspector]
    public abstract class ManagedObjectSO : SerializedScriptableObject
    {
        protected abstract void OnBegin();
        protected abstract void OnEnd();
 
#if UNITY_EDITOR
        protected void OnEnable()
        {
            EditorApplication.playModeStateChanged += OnPlayStateChange;
        }
 
        protected void OnDisable()
        {
            EditorApplication.playModeStateChanged -= OnPlayStateChange;
        }
 
        void OnPlayStateChange(PlayModeStateChange state)
        {
            if(state == PlayModeStateChange.EnteredPlayMode)
            {
                OnBegin();
            }
            else if(state == PlayModeStateChange.ExitingPlayMode)
            {
                OnEnd();
            }
        }
#else
        protected void OnEnable()
        {
            OnBegin();
        }
 
        protected void OnDisable()
        {
            onEnd();
        }
#endif
    }
}