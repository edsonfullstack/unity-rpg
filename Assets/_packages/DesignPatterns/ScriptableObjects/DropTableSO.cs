﻿using System;
using static Logger;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Extensions;

using Sirenix.OdinInspector;
using Unity.Plastic.Newtonsoft.Json;
using UnityEngine;

namespace DesignPatterns.ScriptableObjects
{
    [CreateAssetMenu(fileName = "DropTable", menuName = "ScriptableObjects/Collections/_DropTable")]
    public class DropTableSO : SingletonSO<DropTableSO>
    {
        [ShowInInspector, JsonProperty, BoxGroup("Drop")]
        public static List<DropSO> all { get; set; }
        [ShowInInspector, JsonProperty,BoxGroup("Drop")]
        public static List<DropSO> filtered { get; set; }
        [ShowInInspector,BoxGroup("Drop")]
        public static int allWeight => (int) (all?.Sum(item => item.weight) ?? 0);
        public static int typeWeight(Type type ) => (int) (all?.Sum( item => item.type == type ? item.weight : 0) ?? 0);
        protected override void OnBegin()
        {
            if (this == null) return;
            all = Resources.LoadAll<DropSO>("").ToList();
            all.Sort();
        }

        protected override void OnEnd()
        {
            
        }
        private void OnValidate()
        {
            OnBegin();
        }

        [Button("Random")]
        public void RandomizeDropByType(Type type = null)
        {
            filtered = all.OrderBy(x => x.weight).ToList();
            if (type != null)
                filtered = all.Where(item => item.GetType() == type).OrderBy(x => x.weight).ToList();
            var random = 0.Random(1, filtered.Sum(x => x.weight));
            this.Log(random);
            var result = 0;
            for (var i = 0; i < filtered.Count; i++)
            {
                if (filtered[i].weight < random)
                    random -= (int) all[i].weight;
                else
                {
                    result = i;
                    break;
                }
            }
            Log(JsonConvert.SerializeObject(filtered[result]),this);
        }
    }
    
}