using System;
using System.Linq;
using System.Runtime.CompilerServices;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using Unity.Plastic.Newtonsoft.Json;
using UnityEditor;
using UnityEngine;

namespace DesignPatterns.ScriptableObjects
{
    /// <summary>
    /// Implements a Singleton pattern where u can have only one Instance of the class and can access it by the Class.instace.[field]
    /// </summary>
    [ExecuteAlways]
    public abstract class SingletonSO<T> : ManagedObjectSO where T : SingletonSO<T>
    {
        [ShowInInspector, CanBeNull]
        public static T instance
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get => Resources.LoadAll<T>("ScriptableObjects").FirstOrDefault();
        }
        [DisableInEditorMode] private  int limit { set; get; } = 2;
        
        [ButtonGroup("SingletonSO")]
        [Button("Json")]
        private void LogFind()
        {
            Debug.Log(JsonConvert.SerializeObject(this));
        }
        private void Awake()
        {
            limit = this.name.Equals("") ?  1 : 2;
            if (Resources.LoadAll<T>("ScriptableObjects").Length >= limit)
            {
                DestroyImmediate(this, true);
                throw new NotSupportedException("cant create multiple instances of Singleton SO!");
            }
        }
        
        protected override void OnBegin()
        {
            
        }

        protected override void OnEnd()
        {

        }
    }
}
