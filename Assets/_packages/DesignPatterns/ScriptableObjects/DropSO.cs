﻿using System;
using System.Runtime.CompilerServices;
using Sirenix.OdinInspector;
using Unity.Plastic.Newtonsoft.Json;
using UnityEngine;

namespace DesignPatterns.ScriptableObjects
{
    public abstract class DropSO : ScriptableObject, IComparable<DropSO>
    {
        [ShowInInspector,BoxGroup("Drop Rate")]
        public Type type => this.GetType();
        [ShowInInspector, JsonProperty,BoxGroup("Drop Rate")]
        public uint weight;
        [ShowInInspector, JsonProperty,BoxGroup("Drop Rate")]
        public string chance => $"{(((float)weight / (float)DropTableSO.allWeight) * 100).ToString("##.####")}%";
        [ShowInInspector, JsonProperty,BoxGroup("Drop Rate")]
        public string chanceType => $"{(((float)weight / (float)DropTableSO.typeWeight(this.GetType())) * 100).ToString("##.####")}%";
        
        public int CompareTo(DropSO other)
        {
            // A null value means that this object is greater.
            return (other != null) ? -weight.CompareTo(other.weight) : 1;
        }
    }
}