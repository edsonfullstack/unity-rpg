﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Dynamic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace DesignPatterns
{
    public class Proxy<T> where T : class
    {
        private Dictionary<string, int> registry = new();
        Func<T> func { get; set; }

        public Proxy(Func<T> func)
        {
            this.func = func;
        }
        public T GetProxy()
        {
            return this as T;
        }
        public bool TryInvokeMember(InvokeMemberBinder binder, object[] args, out object result)
        {
            try
            {
                Debug.Log("Invoking " + binder.Name + " with " + string.Join(",",args) + " arguments");
                result = this.GetType().GetMethod(binder.Name).Invoke(this, args);
                if (registry.ContainsKey(binder.Name))
                    registry[binder.Name]++;
                else
                    registry.Add(binder.Name, 1);
                func();
                return true;
            }
            catch
            {
                result = null;
                return false;
            }
        }
    }
}