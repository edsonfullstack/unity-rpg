using UnityEditor.Experimental.GraphView;

namespace Systems.Dialogue.Editor
{
    public class DialogueNode : Node
    {
        public string GUID;
        public string dialogueText;
        public bool entry;
    }
}
