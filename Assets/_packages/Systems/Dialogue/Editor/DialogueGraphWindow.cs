using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

namespace Systems.Dialogue.Editor
{
    public class DialogueGraphWindow : EditorWindow
    {
        private DialogueGraphView _dialogueGraphView;
    
        [MenuItem("Graph/Dialogue Graph")]
        public static void OpenDialogueGraphWindow()
        {
            var window = GetWindow<DialogueGraphWindow>();
            window.titleContent = new GUIContent("Dialogue Graph");
        }
        private void OnEnable()
        {
            ConstructGraphView();
            GenerateToolBar();
        }

        private void OnDisable()
        {
            rootVisualElement.Remove(_dialogueGraphView);
        }

        private void ConstructGraphView()
        {
            _dialogueGraphView = new DialogueGraphView
            {
                name = "Dialogue Graph"
            };
            _dialogueGraphView.StretchToParentSize();
            rootVisualElement.Add(_dialogueGraphView);
        }

        private void GenerateToolBar()
        {
            var toolBar = new Toolbar();
            var buttonCreateNode = new Button(
                () => _dialogueGraphView.CreateNode("Dialogue node name")
            );
            buttonCreateNode.text = "Create Node";
            toolBar.Add(buttonCreateNode);
            rootVisualElement.Add(toolBar);
        }

    
    }
}
