/*
 * https://www.youtube.com/watch?v=7KHGH0fPL84&ab_channel=MertKirimgeri
 */

using System;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;

namespace Systems.Dialogue.Editor
{
    public class DialogueGraphView : GraphView
    {
        private readonly Vector2 nodeDefaultSize = new Vector2(150,200);
        public DialogueGraphView()
        {
            styleSheets.Add(Resources.Load<StyleSheet>("GridStyle"));
            SetupZoom(ContentZoomer.DefaultMinScale,ContentZoomer.DefaultMaxScale);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            var grid = new GridBackground();
            Insert(0,grid);
            grid.StretchToParentSize();
        
            AddElement(GenerateEntryPoint());
        }

        private DialogueNode GenerateEntryPoint()
        {
            var node = new DialogueNode
            {
                title = "Dialogue Start",
                GUID = Guid.NewGuid().ToString(),
                dialogueText = "Dialogue Text",
                entry = true
            };
            //Port
            var outputPort = GeneratePort(node, Direction.Output);
            outputPort.portName = "Next";
            outputPort.portColor = Color.white;
            node.outputContainer.Add(outputPort);
            RefreshState(node);
            //initial position
            node.SetPosition(new Rect(100,200,100,100));
            return node;
        }

        private void RefreshState(DialogueNode node)
        {
            node.RefreshExpandedState();
            node.RefreshPorts();
        }

        private Port GeneratePort(DialogueNode node,Direction direction,Port.Capacity capacity=Port.Capacity.Single)
        {
            return node.InstantiatePort(Orientation.Horizontal, direction, capacity, typeof(string));
        }

        public DialogueNode CreateDialogueNode(string nodeName)
        {
            var node = new DialogueNode
            {
                title = nodeName,
                dialogueText = nodeName,
                GUID = Guid.NewGuid().ToString()
            };
            var inputPort = GeneratePort(node, Direction.Input, Port.Capacity.Multi);
            inputPort.portName = "Input Port";
            inputPort.portColor = Color.green;
            node.inputContainer.Add(inputPort);
            var buttonPortNumber = new Button(() => AddPort(node));
            buttonPortNumber.text = "New Choice";
            node.titleContainer.Add(buttonPortNumber);
            RefreshState(node);
            node.SetPosition(new Rect(Vector2.zero,nodeDefaultSize ));
            return node;
        }

        private void AddPort(DialogueNode node)
        {
            var outputPort = GeneratePort(node, Direction.Output);
            var portNumber = node.outputContainer.Query("connector").ToList().Count;
            outputPort.portName = $"Choice {portNumber}";
            outputPort.portColor = Color.yellow;
            node.outputContainer.Add(outputPort);
            RefreshState(node);
        }

        public void CreateNode(string nodeName)
        {
            AddElement(CreateDialogueNode(nodeName));
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            var compatiblePorts = new List<Port>();
            foreach (var port in ports)
            {
                if(startPort != port && startPort.node != port.node)
                    compatiblePorts.Add(port);
            }
            return compatiblePorts;
        }
    }
}
