﻿using UnityEngine;
    public static class Logger
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="color"></param>
        /// <returns>Use richtext parameters https://docs.unity3d.com/Packages/com.unity.ugui@1.0/manual/StyledText.html</returns>
        public static string Color(this string text, string color)
        {
            return $"<color={color}>{text}</color>";
        }

        private static void DoLog(System.Action<string, Object> LogLibraryFunction, string prefix, Object callerObject,
            params object[] message)
        {
#if UNITY_EDITOR
            var name = (callerObject ? callerObject.name : "Logger").Color("lime");
            LogLibraryFunction($"{prefix}[{name}]: {string.Join("; ", message)}\n ", callerObject);
#endif
        }

        public static void Log(params object[] message)
        {
            DoLog(Debug.Log, "",null, message);
        }
        public static void Log(this Object objectCaller, params object[] message)
        {
            DoLog(Debug.Log, "", objectCaller, message);
        }
        public static void LogError(params object[] message)
        {
            DoLog(Debug.LogError, "<!>".Color("red"), null, message);
        }

        public static void LogError(this Object objectCaller, params object[] message)
        {
            DoLog(Debug.LogError, "<!>".Color("red"), objectCaller, message);
        }
        public static void LogWarning(params object[] message)
        {
            DoLog(Debug.LogWarning, "⚠️".Color("yellow"), null, message);
        }
        public static void LogWarning(this Object myObj, params object[] message)
        {
            DoLog(Debug.LogWarning, "⚠️".Color("yellow"), myObj, message);
        }
        public static void LogSuccess(params object[] message)
        {
            DoLog(Debug.Log, "☻".Color("green"), null, message);
        }

        public static void LogSuccess(this Object myObj, params object[] message)
        {
            DoLog(Debug.Log, "☻".Color("green"), myObj, message);
        }
    }